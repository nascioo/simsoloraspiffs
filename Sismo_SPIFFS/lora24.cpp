/*******************************************************************************************************
  Programs for Arduino - Copyright of the author Stuart Robinson - 06/11/21

  This program is supplied as is, it is up to the user of the program to decide if the program is
  suitable for the intended purpose and free from errors.
*******************************************************************************************************/

/*******************************************************************************************************
  Program Operation - This is a program that transfers a file using data transfer (DT) packet functions
  from the SX128X library to send a file from the SPIFFS card on one Arduino to the SPIFFS card on another Arduino.
  Arduino DUEs were used for the test and this example transfers an JPG image.

  DT packets can be used for transfering large amounts of data in a sequence of packets or segments,
  in a reliable and resiliant way. The file open requests to the remote receiver, each segement sent and
  the remote file close will all keep transmitting until a valid acknowledge comes from the receiver.
  Use this transmitter with the matching receiver program, 234_SPIFFSfile_Transfer_Receiver.ino.

  On transmission the NetworkID and CRC of the payload are appended to the end of the packet by the library
  routines. The use of a NetworkID and CRC ensures that the receiver can validate the packet to a high degree
  of certainty.

  The transmitter sends the sequence of segments in order. If the sequence fails for some reason, the receiver
  will return a NACK packet to the transmitter requesting the segment sequence it was expecting.

  Details of the packet identifiers, header and data lengths and formats used are in the file;
  'Data transfer packet definitions.md' in the \SX128X_examples\DataTransfer\ folder.

  The transfer can be carried out using LoRa packets, max segment size (defined by DTSegmentSize) is 245 bytes
  for LoRa and 117 bytes for FLRC.

  Comment out one of the two following lines at the head of the program to select LoRa or FLRC;

  #define USELORA                              //enable this define to use LoRa packets
  #define USEFLRC                              //enable this define to use FLRC packets

  Serial monitor baud rate is set at 115200.
*******************************************************************************************************/
#include "lora24.h"
#define Program_Version "V1.1"


#include <SPI.h>

#include <SX128XLT.h>
#include <ProgramLT_Definitions.h>
#include "DTSettings.h"                      //LoRa or FLRC settings etc.
#include <arrayRW.h>

SX128XLT LoRa;                               //create an SX128XLT library instance called LoRa

#define PRINTSEGMENTNUM                      //enable this define to print segment numbers 
//#define DEBUG                              //enable this define to print debug info for segment transfers
//#define DEBUGSPIFFS                            //enable this defien to print SPIFFS file debug info
#define ENABLEFILECRC                      //enable this define to uses and show file CRCs
//#define DISABLEPAYLOADCRC                  //enable this define if you want to disable payload CRC checking

//#define USELORA                            //enable this define to use LoRa packets
#define USEFLRC                              //enable this define to use FLRC packets

#define SPIFFSLIB                              //define SPIFFSLIB for SPIFFS.h or SPIFFSFATLIB for SPIFFSfat.h
//#define SPIFFSFATLIB

#include "DTSPIFFSlibrary.h"
#include "DTSLibrary.h"

//choice of files to send         //file length 63091 bytes, file CRC 0x59CE
#include "SPIFFS.h"


bool lora24::lora24tx(String buf)
{
  Serial.println(("Transfer started"));
  int len = buf.length()+1;
  char DTFileName[len];
  buf.toCharArray(DTFileName, len);
  bool tx = true;
  bool rx = true;
  do
  {
    DTStartmS = millis();

    //opens the local file to send and sets up transfer parameters
    if (startFileTransfer(DTFileName, sizeof(DTFileName), DTSendAttempts))
    {
      Serial.print(DTFileName);
      Serial.println(F(" opened OK on remote"));
      printLocalFileDetails();
      Serial.println();
      NoAckCount = 0;
    }
    else
    {
      Serial.print(DTFileName);
      Serial.println(F("  Error opening remote file - restart transfer"));
      DTFileTransferComplete = false;
      if(NoAckCount>12)
      {
        rx = false;
        DTFileTransferComplete = true;
      }

    }

    delay(packetdelaymS);
    if(rx == true)
    {
    if (!sendSegments())
    {
      Serial.println();
      Serial.println(F("**********************************************************"));
      Serial.println(F("Error - Segment write with no file open - Restart received"));
      Serial.println(F("**********************************************************"));
      Serial.println();
      continue;
      return false;
    }
    if(NoAckCount>50)
      {
        rx = false;
        DTFileTransferComplete = true;
      }

    if (endFileTransfer(DTFileName, sizeof(DTFileName)))         //send command to close remote file
    {
      DTSendmS = millis() - DTStartmS;                  //record time taken for transfer
      Serial.print(DTFileName);
      Serial.println(F(" closed OK on remote"));
      beginarrayRW(DTheader, 4);
      DTDestinationFileLength = arrayReadUint32();
      Serial.print(F("Acknowledged remote destination file length "));
      Serial.println(DTDestinationFileLength);
      if (DTDestinationFileLength != DTSourceFileLength)
      {
        Serial.println(F("ERROR - file lengths do not match"));
        rx = false;
      }
      else
      {
        Serial.println(F("File lengths match"));
      }
#ifdef ENABLEFILECRC
      DTDestinationFileCRC = arrayReadUint16();
      Serial.print(F("Acknowledged remote destination file CRC 0x"));
      Serial.println(DTDestinationFileCRC, HEX);
      if (DTDestinationFileCRC != DTSourceFileCRC)
      {
        Serial.println(F("ERROR - file CRCs do not match"));
        return false;
      }
      else
      {
        Serial.println(F("File CRCs match"));
        rx = true;
        break;
      }
#endif
      DTFileTransferComplete = true;
    }
    else
    {
      DTFileTransferComplete = true;
      Serial.println(F("ERROR send close remote destination file failed - program halted"));
      rx = false;
    }
    rx = false;
  }
  }
  while (!DTFileTransferComplete);


  Serial.print(F("NoAckCount "));
  Serial.println( NoAckCount);
  Serial.println();

  DTsendSecs = (float) DTSendmS / 1000;
  Serial.print(F("Transmit time "));
  Serial.print(DTsendSecs, 3);
  Serial.println(F("secs"));
  Serial.print(F("Transmit rate "));
  Serial.print( (DTDestinationFileLength * 8) / (DTsendSecs), 0 );
  Serial.println(F("bps"));
  Serial.println(("Transfer finished"));

  Serial.println(("Program halted"));
  tx = false;
  while (tx);
  return rx;
}


void lora24::led_Flash(uint16_t flashes, uint16_t delaymS)
{
  uint16_t index;
  for (index = 1; index <= flashes; index++)
  {
    digitalWrite(LED1, HIGH);
    delay(delaymS);
    digitalWrite(LED1, LOW);
    delay(delaymS);
  }
}


void lora24::initialize()
{
  pinMode(LED1, OUTPUT);                          //setup pin as output for indicator LED
  led_Flash(2, 125);                              //two quick LED flashes to indicate program start
  setDTLED(LED1);                                 //setup LED pin for data transfer indicator


  Serial.println(F(__FILE__));
  Serial.flush();

  SPI.begin();

  if (LoRa.begin(NSS, NRESET, RFBUSY, DIO1, LORA_DEVICE))
  {
    led_Flash(2, 125);
  }
  else
  {
    Serial.println(F("LoRa device error"));
    while (1)
    {
      led_Flash(50, 50);                          //long fast speed flash indicates device error
    }
  }

  
#ifdef USELORA
  LoRa.setupLoRa(Frequency, Offset, SpreadingFactor, Bandwidth, CodeRate);
  Serial.println(F("Using LoRa packets"));
#endif

#ifdef USEFLRC
  LoRa.setupFLRC(Frequency, Offset, BandwidthBitRate, CodingRate, BT, Syncword);
  Serial.println(F("Using FLRC packets"));
#endif

  LoRa.printOperatingSettings();
  Serial.println();
  LoRa.printModemSettings();
  Serial.println();

  Serial.print(F("Initializing SPIFFS card..."));

  if (DTSPIFFS_initSPIFFS())
  {
    Serial.println(F("SPIFFS Card initialized."));
  }
  else
  {
    Serial.println(F("SPIFFS Card failed, or not present."));
    while (1) led_Flash(100, 25);
  }

  Serial.println();
  DTSPIFFS_printDirectory();
  Serial.println();

#ifdef DISABLEPAYLOADCRC
  LoRa.setReliableConfig(NoReliableCRC);
#endif

  if (LoRa.getReliableConfig(NoReliableCRC))
  {
    Serial.println(F("Payload CRC disabled"));
  }
  else
  {
    Serial.println(F("Payload CRC enabled"));
  }

  DTFileTransferComplete = false;

  Serial.println(F("SPIFFSfile transfer ready"));
  Serial.println();
}
