
//*****************************************************************
/*----------------PROGRAMMAZIONE IMPOSTAZIONI--------------------
 vedi Foto in cartella per impostazioni IDE
 * 
******************************************************************/
//--------------function declaration--------------
//void reset_accelerometer_imu(void);
void diagnostica(void);
void orologio(void);
void imposta_puntatore_wifi(void);
void rx_pack(void);
#include <Arduino.h>
//------------------------------------------------
#include "lora24.h"
#include <MPU6050_tockn.h>
#include "SPIFFS.h"
#include <Wire.h>
#include "esp_system.h"
#include <Preferences.h>
#define PROGRAM_NUM 105
Preferences preferences;
#define PRIMA_PROG

//parametri salvari in EEPROM dopo prima programmazione
//----------------------da scommentare solo prima prog------------------
//------------------------------------------------------------------

/******************************PARAMETRI DA VARIARE*****************************/
//#define N_PORT 3030+N_SERIAL
#define NUM_EVENTI_PACK 50
#define NUM_PIC_PACK 80
#define SOGLIA 1.5
#define DELAY_UDP_TX 1//50  //tempo che dipende dal ping sotto 100ms è un po' inutile mi sa
/*******************************************************************************/
#define PIGR 3.14159
#define CON_CONV_BAT 0.001  //3.3v/2^12bit = VADC = 0.0008 *4.3M/3.3M partitore = 0.001 fattore conversione da digit misurati e valore batteria
#define BAT A5
#define TIME_OUT_MS 200
#define N_RETX 5
#define CSPIN 4   //pin abilitazione memoria flash per evitare comunicazione con la radio ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¨ il pin 4
#define FREQ_DLPF_MEMS 0x02 //CON DLPF  0-260hz 1-184hz 2-94hz 3-44hz 4-21hz 5-10hz 6-5hz
//------------------------**tempi e cicli da impostare**------------------------------
//---------------TIME------------------
#define TIME_POST_TRIGGER 4
#define TIME_PRE_TRIGGER 1
#define TIME_DIAGNOSTICA 15 //internvallo in MINUTI
#define DIM_PRETR 700   //circa 1s
#define DIM_MEM_EVENT DIM_PRETR*(TIME_POST_TRIGGER+1)
#define F_CAMP 1000   //micro secondi = 1/500u = 2kH
#define TENT_CHAN_LOAD (50+(N_SERIAL*3))
#define ACC_FULL MPU6050_ACCEL_FS_2
#define GYR_FULL MPU6050_GYRO_FS_250
#define DIMEN_FILE 42000//350000//DIMEN_FILE in byte non in bit -- 16bit = 2byte 2*x-y-z-temp = 2*4=8byte * 1000*5s = 40000 sicurezza 40500  memo 2Mbyte / 42k = 47 eventi max -- in realtÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â  28 max quindi facciamo 25 e siamo sicuri
#define NUM_WIFI_FAULT TIME_DIAGNOSTICA*60  //calcolo in secondi si resetta se salta la trasmissione di una diagnostica
//------------------------------------------------------------------------------------
#define DIM_PACK 250 //RH_RF95_MAX_MESSAGE_LEN //255 dovrebbe essere
#define CS_MEM 4
//********************************************/
#define RADICE_DATA 1100
#define RADICE_DIAGNOS 1200 
#define CLIENT_ADDRESS 2
#define RADICE_TEST 1300   
#define SERVER_ADDRESS 250
#define N_SERIAL 1003
#define TIMEOUT_PKT_RADIO 600
#define NUM_RETRY 5
#define VREF 8
#define TIME_CAMP_ANALOG 60
#define AN_1 A1
#define AN_2 A2
#define AN_3 A3
#define SUPP_ACC 9
#define TIMEOUT_RADIO_CRASH 40  //in minuti
#define SPIKE 10000
#define GRAVITY_MM 9806.65 //e trasformazione in mm/s^2
//*********************************************/
//-----------acc -- vel --> V=Acc/(2pif)
lora24 lora;
MPU6050 accelgyro(Wire);
//RH_RF95 driver(5, 2); // Rocket Scream Mini Ultra Pro with the RFM95W
//RH_RF95 driver;
//RHReliableDatagram manager(driver, CLIENT_ADDRESS); //oggetto manager quello che usi per la comunicazione si occupa del (SW)
int tentativi = 0;
File file2;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high
uint8_t dato_pack,wifi_resp_count,first_diagnostica;
const int wdtTimeout = 30000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;
uint16_t puntatore_acc_cont, count_pretrigger, puntatore_wifi_send;
uint16_t time_from_packet_send;
int16_t x,y,z;//,gx,gy,gz;
int16_t off_x,off_y,off_z;
int16_t x_prec, y_prec, z_prec;
uint16_t count_integr_x,count_integr_y,count_integr_z;
float vel_memo_x, max_vel_x;
float vel_memo_y, max_vel_y;
float vel_memo_z, max_vel_z;
int16_t max_x, max_y, max_z;
uint16_t time_event[DIM_MEM_EVENT];
int16_t acc_cont_x[DIM_MEM_EVENT];
int16_t acc_cont_y[DIM_MEM_EVENT];
int16_t acc_cont_z[DIM_MEM_EVENT];

unsigned long t_prec_x, t_prec_y, t_prec_z, t_campion, t_handle;
float t_wave_x, t_wave_y, t_wave_z;
uint8_t count_taratura;
uint8_t n_packet;
uint16_t serial_num, radice;
int8_t num_file;
uint8_t id[5];
uint32_t data_1_memo, data_2_memo, data_3_memo;
//SerialFlashFile file;
char name_file[7];
//variable gps
unsigned long milli_s_prec;
unsigned long milli_s;
uint16_t ore;
uint16_t minuti;
uint16_t secondi;
uint16_t giorni;
uint16_t mesi;
uint16_t anni;
int latitudine;
int longitudine;
int16_t altitudine;
//uint16_t count_wdt_reset;
uint8_t count_analog;
uint16_t time_post_trigger, time_pre_trigger;
uint16_t time_post_event;
uint16_t time_debug;
uint16_t time_analog_read;
uint16_t time_gps_refresh;
uint8_t time_post_event_enable;
uint8_t time_post_trigger_enable;
uint16_t memo_time, memo_time_handle;
uint8_t new_dato;
uint8_t post_trigger_on, file_tutti_creati;
uint8_t soglia;
uint16_t count_spike;
uint16_t num_evento;
uint16_t time_diagn;
uint16_t n_porta;
uint16_t n_serial;
String startmessage;
String endmessage;
uint8_t eventi_memoria;
int n_elements = 20;
String namefile = String(N_SERIAL);


 





void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart();
}


void logMemory() {
  log_d("Used PSRAM: %d", ESP.getPsramSize() - ESP.getFreePsram());
}

void setup(){
   Wire.begin();   //senza address = master
  Wire.setClock(300000);   //il max che si riesce a tirare fuori con la libreria e con l'MPU6050 sono 541uS ogni campione = max 1848Hz di campionamento
  Serial.begin(115200);
  //on watchdog
  //sodaq_wdt_enable(WDT_PERIOD_8X);
  // join I2C bus (I2Cdev library doesn't do this automatically)
    if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  SPIFFS.format();
//************************************MEMO EEPROM*************************************
  preferences.begin("func_ser",false);
  #ifdef PRIMA_PROG
    serial_num = N_SERIAL;
    preferences.putUInt("numb_ser",serial_num);
    
  #endif    
  lora.initialize();
  serial_num = preferences.getUInt("numb_ser",0);
 
  preferences.end();  
 
//***************************RESET ACCELEROMETER ALL'AVVIO PER EVITARE BLOCCHI DEL SISTEMA********************************/
  pinMode(SUPP_ACC, OUTPUT);

  accelgyro.begin();

  accelgyro.calcGyroOffsets(true);
  Serial.println(DIM_PACK);

  //initialize
  x_prec = 0;
  y_prec = 0;
  z_prec = 0;
  t_wave_x = 0;
  t_wave_y = 0;
  t_wave_z = 0;
  num_evento = 1;
  t_prec_x = micros();
  t_prec_y = micros();
  t_prec_z = micros();
  t_campion = micros();
  t_handle = millis();
  milli_s = 0;
  milli_s_prec = millis();
  count_integr_x = 0;
  count_integr_y = 0;
  count_integr_z = 0;
  vel_memo_x = 0;
  vel_memo_y = 0;
  vel_memo_z = 0;
  max_x = 0;
  time_diagn = 0;
  max_y = 0;
  max_z = 0;
  n_packet = 0;
  post_trigger_on = 0;
  puntatore_acc_cont = 0;
  puntatore_wifi_send = 0;
  count_pretrigger = 0;
  //count_wdt_reset = 0;
  for(uint16_t i = 0; i < DIM_MEM_EVENT; i++){
    acc_cont_x[i] = 0;
    acc_cont_y[i] = 0;
    acc_cont_z[i] = 0;
    time_event[i] = 0;
  }
  //timer
  time_post_trigger = 0;
  time_pre_trigger = 0;
  time_debug = 0;
  time_post_event_enable = 0;
  time_post_trigger_enable = 0;
  new_dato = 0; 
  memo_time = 0;
  memo_time_handle = 0;
  data_1_memo = 0;
  data_2_memo = 0;
  data_3_memo = 0;
  time_analog_read = 0;
  count_analog = 0;
  count_spike = 0;
  off_x = 0;
  off_y = 0;
  off_z = -10000;
  max_vel_x = 0.00;
  max_vel_y = 0.00;
  max_vel_z = 0.00;
  wifi_resp_count = 0;
  first_diagnostica = 0;
  eventi_memoria = 0;
//  pic_x_1 = "";
//  pic_y_1 = "";
//  pic_z_1 = "";
//  pic_x_2 = "";
//  pic_y_2 = "";
//  pic_z_2 = "";
//  pic_x_3 = "";
//  pic_y_3 = "";
//  pic_z_3 = "";
  Serial.print("num_serie: 1X0");
  Serial.println(serial_num);
  
  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);                          //enable interrupt

  /*PROGRAMMING SERVER PAGE FOR ESP32 CONFIGURATION*/
  /*return index page which is stored in serverIndex */
 
  //settaggio DLPF filtro LPF su MPU6050
  delay(200);
  accelgyro.writeMPU6050(MPU6050_CONFIG, FREQ_DLPF_MEMS);
  delay(200);
   diagnostica();
  

} // setup
 
void loop(){
  //aggiorna valori accelerometro e relativo offset
  accelgyro.update();
  x = accelgyro.getAccX()*GRAVITY_MM + off_x;
  y = accelgyro.getAccY()*GRAVITY_MM + off_y;
  z = accelgyro.getAccZ()*GRAVITY_MM + off_z;
  
  //compensate offset
  if (x > 0) off_x--; else if (x < 0) off_x++;
  if (y > 0) off_y--; else if (y < 0) off_y++;
  if (z > 0) off_z--; else if (z < 0) off_z++;
  //watchdog reset
  timerWrite(timer, 0); //reset timer (feed watchdog)
  //se perde connessione wifi 
  
  /*********************CAMPIONAMENTO CONTINUO ACCELERAZIONE E TEMPO FRA UN CAMPIONE E L'ALTRO***************************/
  memo_time = (uint16_t) abs(micros()-t_campion);
  memo_time_handle = (uint16_t) abs(millis()-t_handle);
  if(memo_time>=F_CAMP){    
    if(memo_time_handle>10){
      timerAlarmDisable(timer);
      timerAlarmEnable(timer);
      timerWrite(timer, 0); //reset timer (feed watchdog)   
      t_handle = micros();   
    }
    orologio();
    new_dato = 100;
  
    //Serial.println(micros()-t_campion);
    t_campion = micros();
    puntatore_acc_cont++;
    if(puntatore_acc_cont>=DIM_MEM_EVENT-1){
      puntatore_acc_cont=0;
    }
    puntatore_wifi_send++;
    if(puntatore_wifi_send>=DIM_MEM_EVENT-1){
      puntatore_wifi_send=0;
    }        
    acc_cont_x[puntatore_acc_cont] = x;
    acc_cont_y[puntatore_acc_cont] = y;
    acc_cont_z[puntatore_acc_cont] = z;
    time_event[puntatore_acc_cont] = memo_time; //+ 30000;
  }
  /**************VALORE SOGLIA CON DIN 4150 CONTINUA*******************************/
  //blocca l'acquisizione di nuovi eventi fin che non si ÃƒÆ’Ã‚Â¨ liberata la memoria
  if(!post_trigger_on){
    //search zero crossing
    t_wave_x = (float) abs(micros() - t_prec_x);
    t_wave_y = (float) abs(micros() - t_prec_y);
    t_wave_z = (float) abs(micros() - t_prec_z);  
    if((((x_prec>0)&&(acc_cont_x[puntatore_acc_cont]<=0))||((x_prec<0)&&(acc_cont_x[puntatore_acc_cont]>=0)))&&(!post_trigger_on)){
      uint16_t freq_x = (1/(2*(t_wave_x)/1000000));
      vel_memo_x = ((max_x*2*t_wave_x)/(2*PIGR*1000000));
      if((abs(vel_memo_x)>max_vel_x)&&(freq_x>0)){
        max_vel_x = abs(vel_memo_x);
      }
      if((vel_memo_x>SOGLIA)&&(freq_x>0)&&(time_pre_trigger>=TIME_PRE_TRIGGER)){
        Serial.print("MAX_X: ");
        Serial.println(vel_memo_x);
        Serial.print("FREQ_X: ");
        Serial.println(freq_x);
        vel_memo_x = 0;
        imposta_puntatore_wifi();
        post_trigger_on = true;
        time_post_trigger = 0;
      }
      //clear variable and upgrade state
      t_prec_x = micros();
      max_x = 0;
    }
    
    if((((y_prec>0)&&(acc_cont_y[puntatore_acc_cont]<=0))||((y_prec<0)&&(acc_cont_y[puntatore_acc_cont]>=0)))&&(!post_trigger_on)){
      uint16_t freq_y = (1/(2*(t_wave_y)/1000000));
      vel_memo_y = ((max_y*2*t_wave_y)/(2*PIGR*1000000));
      if((abs(vel_memo_y)>max_vel_y)&&(freq_y>0)){
        max_vel_y = abs(vel_memo_y);
      }
      if((vel_memo_y>SOGLIA)&&(freq_y>0)&&(time_pre_trigger>=TIME_PRE_TRIGGER)){
        Serial.print("MAX_Y: ");
        Serial.println(vel_memo_y);
        Serial.print("FREQ_Y: ");
        Serial.println(freq_y);        
        vel_memo_y = 0;
        imposta_puntatore_wifi();
        post_trigger_on = true;
        time_post_trigger = 0;
      }
      //clear variable and upgrade state
      t_prec_y = micros();
      max_y = 0;
    }
    if((((z_prec>0)&&(acc_cont_z[puntatore_acc_cont]<=0))||((z_prec<0)&&(acc_cont_z[puntatore_acc_cont]>=0)))&&(!post_trigger_on)){
      uint16_t freq_z = (1/(2*(t_wave_z)/1000000));
      vel_memo_z = ((max_z*2*t_wave_z)/(2*PIGR*1000000));
      if((abs(vel_memo_z)>max_vel_z)&&(freq_z>0)){
        max_vel_z = abs(vel_memo_z);
      }
      if((vel_memo_z>SOGLIA)&&(freq_z>0)&&(time_pre_trigger>=TIME_PRE_TRIGGER)){
        Serial.print("MAX_Z: ");
        Serial.println(vel_memo_z);
        Serial.print("FREQ_Z: ");
        Serial.println(freq_z);        
        vel_memo_z = 0;
        imposta_puntatore_wifi();
        post_trigger_on = true;
        time_post_trigger = 0;
      }
      //clear variable and upgrade state
      t_prec_z = micros();
      max_z = 0;
    }
    if(max_x<abs(acc_cont_x[puntatore_acc_cont])){
      max_x = abs(acc_cont_x[puntatore_acc_cont]);
    }
    if(max_y<abs(acc_cont_y[puntatore_acc_cont])){
      max_y = abs(acc_cont_y[puntatore_acc_cont]);
    }
    if(max_z<abs(acc_cont_z[puntatore_acc_cont])){
      max_z = abs(acc_cont_z[puntatore_acc_cont]);
    }
  }
  //aggiornamento stati
  x_prec = acc_cont_x[puntatore_acc_cont];
  y_prec = acc_cont_y[puntatore_acc_cont];
  z_prec = acc_cont_z[puntatore_acc_cont];

  /********************TRASMISSIONE DATI lora******************/
  
  if((time_post_trigger>=TIME_POST_TRIGGER)&&(post_trigger_on)){
    compress_event();
    Serial.print("invio lora");
    sendevent();

  /********************TRASMISSIONE DATI lora******************/
    

} 
  //AGGIUNGERE DIAGNOSTICA A TEMPO E IN CORRISPONDENZA DELLA QUALE FA RESET E TEST DELLE MEMS
  
  if(time_diagn >= TIME_DIAGNOSTICA){
    
    diagnostica();
    time_diagn=0;
  }
}// loop
/***********************IMPOSTARE PUNTATORE WIFI*******************************/
void imposta_puntatore_wifi(void){
  puntatore_wifi_send = puntatore_acc_cont-DIM_PRETR;
  if(puntatore_wifi_send<0){
    puntatore_wifi_send=DIM_MEM_EVENT+puntatore_wifi_send-1;
  }
  return;
}
/***********************INVIO OGNI X TEMPO DATI DIAGNOSTICA***************************/
void diagnostica(void){
  timerWrite(timer, 0);
  float battery_level = 0;
  accelgyro.begin();
  dato_pack = '0'; //scanzela la variabile temporanea
  String memo_pack_send = "DIAGNOSTICA,"+String(serial_num+RADICE_DIAGNOS)+",Bat,"+String(battery_level)+",Temp,"+String(accelgyro.getTemp())+",L_ev,"+String(num_evento)+",S_mms,"+String(SOGLIA)+",P_n,"+String(PROGRAM_NUM)+",MX,"+String(max_vel_x)+",MY,"+String(max_vel_y)+",MZ,"+String(max_vel_z)+",memX,"+String(accelgyro.getAccTestX())+",memY,"+String(accelgyro.getAccTestY())+",memZ,"+String(accelgyro.getAccTestZ())/*+",mems_gx,"+String(accelgyro.getGyroTestX())+",mems_gy,"+String(accelgyro.getGyroTestY())+",mems_gz,"+String(accelgyro.getGyroTestZ())*/+'\n';
  //invio diagnostica prova 17-05
  writefile(namefile + "diagnostica.txt",memo_pack_send);
  Serial.print("scrivo file diagnostica");
  delay(500);
  lora.lora24tx("/"+ namefile + "diagnostica.txt");
   //fine invio diagnostica prova 17-05
  //Aggiungere conferma della diagnostica altrimenti encuca del wifi non si sblocca
  accelgyro.calcGyroOffsets(true);
  //RITARDO E 2 CONTROLLO PER EVENTUALE PERDITA PACCHETTI ANCHE AL TX TRASMETTO 2 VOLTE OK
  max_vel_x=0.00;
  max_vel_y=0.00;
  max_vel_z=0.00;
  timerWrite(timer, 0);
  if(dato_pack=='K'){
    Serial.write('O');   
    dato_pack = '0'; //scanzela la variabile temporanea
  }
  else{
  }
  timerWrite(timer, 0);
  first_diagnostica = 100;
  //settaggio filtro che non cambi con autotest delle mems
  delay(200);
  accelgyro.writeMPU6050(MPU6050_CONFIG, FREQ_DLPF_MEMS);
  delay(200);
  return;
  
}
void orologio(void){
  milli_s = abs(millis() - milli_s_prec);
  if(milli_s>999){
    timerWrite(timer, 0);
    secondi++;
    milli_s_prec = millis();
    milli_s = 0;
    time_post_trigger++;
    time_pre_trigger++;
    time_post_event++;
    time_debug++;
    time_gps_refresh++;
    time_analog_read++;
  }
  if(secondi>59){
    minuti++;
    time_diagn++;
    time_from_packet_send++;
    wifi_resp_count++;
    secondi = 0;    
  }
  if(minuti>59){
    ore++;
    minuti = 0;        
  }
  if(ore>23){
    minuti++;
    ore = 0;        
  }
  return;
}

void compress_event(){
  //compressione dei dati tramite decimazione si tengono solo picchi max e min con rispettivo tempo
  uint16_t i = puntatore_acc_cont+1;
  int16_t memo_acc_x = acc_cont_x[i];
  int16_t memo_acc_y = acc_cont_y[i];
  int16_t memo_acc_z = acc_cont_z[i];
  int16_t memo_pic_x = 0;
  int16_t memo_pic_y = 0;
  int16_t memo_pic_z = 0;
  uint32_t tempo_grafico_us = 0;
  uint8_t count_leng_pack_x = 0;
  uint8_t count_leng_pack_y = 0;
  uint8_t count_leng_pack_z = 0;
  String pic_x, pic_y, pic_z, pic_all;
  eventi_memoria++;
  bool max_trovato_x = false;
  bool max_trovato_y = false;
  bool max_trovato_z = false;
  String memo_send = String(RADICE_DATA+serial_num) + ',' + String(num_evento) + ';';
  //loop da puntatore array circolare alla fine
  for(i = puntatore_acc_cont+2; i < DIM_MEM_EVENT; i++){
    if((abs(memo_acc_x)>abs(acc_cont_x[i]))&&(!max_trovato_x)){
        memo_pic_x = memo_acc_x;
        max_trovato_x = true;
      }
      //cerca passaggio da 0
      if((memo_acc_x<0)&&(acc_cont_x[i]>0)||(memo_acc_x>0)&&(acc_cont_x[i]<0)){
        max_trovato_x = false;
      }
    if((abs(memo_acc_z)>abs(acc_cont_z[i]))&&(!max_trovato_z)){
        memo_pic_z = memo_acc_z;
        max_trovato_z = true;
      }
      //cerca passaggio da 0
      if((memo_acc_z<0)&&(acc_cont_z[i]>0)||(memo_acc_z>0)&&(acc_cont_z[i]<0)){
        max_trovato_z = false;
      }
    if((abs(memo_acc_y)>abs(acc_cont_y[i]))&&(!max_trovato_y)){
        memo_pic_y = memo_acc_y;
        max_trovato_y = true;
      }
      //cerca passaggio da 0
      if((memo_acc_y<0)&&(acc_cont_y[i]>0)||(memo_acc_y>0)&&(acc_cont_y[i]<0)){
        max_trovato_y = false;
      }
    if(memo_pic_x!=0){
      String memos = String(memo_pic_x)+ ',' + String(tempo_grafico_us) + ",x\n";
        pic_all+=memos;
    }
    if(memo_pic_y!=0){
      String memos = String(memo_pic_y)+ ',' + String(tempo_grafico_us) + ",y\n";
        pic_all+=memos;      
    }
    if(memo_pic_z!=0){
      String memos = String(memo_pic_z)+ ',' + String(tempo_grafico_us) + ",z\n";
      
        pic_all+=memos;      
    }
    tempo_grafico_us += time_event[i];
    memo_acc_x = acc_cont_x[i];
    memo_acc_y = acc_cont_y[i];
    memo_acc_z = acc_cont_z[i];
    memo_pic_x = 0;
    memo_pic_y = 0;
    memo_pic_z = 0;
    timerWrite(timer, 0); //reset timer (feed watchdog)
  }
  //loop da inizio array circolare a puntatore
  for(i = 0; i < puntatore_acc_cont+2; i++){
     if((abs(memo_acc_x)>abs(acc_cont_x[i]))&&(!max_trovato_x)){
        memo_pic_x = memo_acc_x;
        max_trovato_x = true;
      }
      //cerca passaggio da 0
      if((memo_acc_x<0)&&(acc_cont_x[i]>0)||(memo_acc_x>0)&&(acc_cont_x[i]<0)){
        max_trovato_x = false;
      }
    if((abs(memo_acc_z)>abs(acc_cont_z[i]))&&(!max_trovato_z)){
        memo_pic_z = memo_acc_z;
        max_trovato_z = true;
      }
      //cerca passaggio da 0
      if((memo_acc_z<0)&&(acc_cont_z[i]>0)||(memo_acc_z>0)&&(acc_cont_z[i]<0)){
        max_trovato_z = false;
      }
    if((abs(memo_acc_y)>abs(acc_cont_y[i]))&&(!max_trovato_y)){
        memo_pic_y = memo_acc_y;
        max_trovato_y = true;
      }
      //cerca passaggio da 0
      if((memo_acc_y<0)&&(acc_cont_y[i]>0)||(memo_acc_y>0)&&(acc_cont_y[i]<0)){
        max_trovato_y = false;
      }
    if(memo_pic_x!=0){
      String memos = String(memo_pic_x)+ ',' + String(tempo_grafico_us) + ",x\n";
      count_leng_pack_x += memos.length();
      pic_all+=memos;
    }
    if(memo_pic_y!=0){
      String memos = String(memo_pic_y)+ ',' + String(tempo_grafico_us) + ",y\n";
      count_leng_pack_y += memos.length();
      pic_all+=memos;    
    }
    if(memo_pic_z!=0){
      String memos = String(memo_pic_z)+ ',' + String(tempo_grafico_us) + ",z\n";
      count_leng_pack_z += memos.length();
      pic_all+=memos;      
    }
    tempo_grafico_us += time_event[i];
    memo_acc_x = acc_cont_x[i];
    memo_acc_y = acc_cont_y[i];
    memo_acc_z = acc_cont_z[i];
    memo_pic_x = 0;
    memo_pic_y = 0;
    memo_pic_z = 0;      
    timerWrite(timer, 0); //reset timer (feed watchdog)
  }
writefile(namefile + "pic_"+eventi_memoria+".txt", pic_all );
  
  time_post_trigger = 0;
  time_pre_trigger = 0;
  post_trigger_on = false;
  return;
}


void sendevent(void){ 
  Serial.print(eventi_memoria + " eventi da inviare");
  for(int i = eventi_memoria; i > 0 ; i--)
  {
    Serial.print("invio x"+ eventi_memoria);
    if(lora.lora24tx("/"+namefile + "pic_"+i+".txt"))
    {
              eventi_memoria--;
              delay(2000);
              SPIFFS.remove("/"+namefile + "pic_"+i+".txt");
              tentativi = 0;
    }
    else
    {
      if(tentativi<3)
      {
       Serial.print("tentativo");
         timerWrite(timer, 0);
        tentativi++;
        delay(5000);
        sendevent();
      }
      else
      {
        break;
      }
    }
  }
    
    tentativi = 0;
  
}

char* tochar(String myString)
{

  char buf[myString.length()+1];
  myString.toCharArray(buf, myString.length() + 1);
  return buf;

}

void writefile(String namefile, String stringa)
{
  bool writed =false;
  while(writed==false)
  {
    File file;
     file = SPIFFS.open("/"+namefile, "a");
     
  if (!file) {
    Serial.println("There was an error opening the file for writing");
    return;
  }
  if (file.print(stringa)) {
    Serial.println("File "+ namefile +" was written");
    writed=true;
  } else {
    Serial.println("File "+ namefile +" write failed");
  }
  file.close();
  } 
delay(100);
}
