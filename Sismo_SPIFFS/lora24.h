#ifndef LORA24_h
#define LORA24_h
#include <Wire.h>
#include "esp_system.h"
#include <SPI.h>   
class lora24
{

   public:
      void initialize ();
      bool lora24tx(String);
      void led_Flash(uint16_t, uint16_t);

};

#endif
